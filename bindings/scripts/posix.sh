#!/bin/sh

curl -z master.zip -o master.zip -L https://github.com/ocornut/imgui/archive/master.zip

SCRIPTS_DIR=`pwd`
mkdir -p $1 $1/include $1/lib
unzip -d $1 -o master.zip
cd $1/imgui-master
clang++ -c *.cpp
libtool -static -o $1/lib/libimgui.a *.o
mv *.h $1/include
cd $1/include
patch < $SCRIPTS_DIR/v1.49.patch
