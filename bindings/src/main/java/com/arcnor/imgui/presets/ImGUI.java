package com.arcnor.imgui.presets;

import org.bytedeco.javacpp.annotation.Platform;
import org.bytedeco.javacpp.annotation.Properties;
import org.bytedeco.javacpp.tools.Info;
import org.bytedeco.javacpp.tools.InfoMap;
import org.bytedeco.javacpp.tools.InfoMapper;

@Properties(target = "com.arcnor.imgui.ImGUI", value = {@Platform(include = {"imgui.h", "imgui_internal.h"}, link = "imgui")})
public class ImGUI implements InfoMapper {
	public void map(InfoMap infoMap) {
		infoMap
				.put(new Info("char* (*)()").pointerTypes("GetClipboardTextFn"))    // FIXME: This is just a hack
				.put(new Info("STB_TEXTEDIT_STRING", "STB_TEXTEDIT_CHARTYPE", "IMGUI_ONCE_UPON_A_FRAME").skip())
				.put(new Info("IMGUI_DISABLE_OBSOLETE_FUNCTIONS").define())
				.put(new Info("IMGUI_DEFINE_MATH_OPERATORS").define(false))
				.put(new Info("ImPlacementNewDummy").skip())
				.put(new Info("unsigned short").javaNames("short").valueTypes("short").pointerTypes("ShortPointer", "ShortBuffer", "short[]"))
				.put(new Info("ImGui::TreePush", "ImGui::TreePush()", "ImGui::TreePush(const char* str_id = NULL);").skip())
				.put(new Info("ImGuiTextEditState::StbState").skip())
				.put(new Info("ImWchar", "ImDrawIdx").cast().valueTypes("short").pointerTypes("ShortPointer", "ShortBuffer", "short[]"));
		String[] innerClasses = {
				"ImGuiTextFilter::TextRange",
				"ImGuiStorage::Pair",
				"ImFont::Glyph"
		};

		for (String innerClass : innerClasses) {
			final String javaName = innerClass.replace("::", ".");
			infoMap.put(new Info(innerClass).javaNames(javaName).valueTypes(javaName, javaName, javaName).pointerTypes(javaName, javaName, javaName));
		}

		String[] imVectors = {
				"ImFont::Glyph",
				"ImDrawChannel",
				"ImDrawCmd",
				"ImDrawIdx",
				"ImDrawList*",
				"ImDrawVert",
				"ImFont*",
				"ImFontConfig",
				"ImGuiColMod",
				"ImGuiColumnData",
				"ImGuiGroupData",
				"ImGuiID",
				"ImGuiIniData",
				"ImGuiPopupRef",
				"ImGuiStorage::Pair",
				"ImGuiStyleMod",
				"ImGuiWindow*",
				"ImTextureID",
				"ImVec2",
				"ImVec4",
				"ImWchar",
				"ImGuiStorage::Pair",
				"ImGuiTextFilter::TextRange",
				"bool",
				"char",
				"float",
				"int",
				"short",
		};

		infoMap.put(new Info("ImVector<unsigned short>").pointerTypes("ImVector_unsigned_short").define());
		infoMap.put(new Info("ImVector<unsigned short>::erase", "ImVector<unsigned short>::insert", "ImVector<unsigned short>::begin", "ImVector<unsigned short>::end").skip());
		infoMap.put(new Info("ImVector<unsigned short>::Data").annotations("@Cast(\"unsigned short*\")"));

		for (String imVector : imVectors) {
			final String newName = imVector.replace("*", "Ptr").replace("::", "_");
			infoMap.put(new Info("ImVector<" + imVector + ">").pointerTypes("ImVector_" + newName).define());

			// FIXME
//			if (
//					imVector.equals("short") || imVector.equals("bool") || imVector.equals("float") || imVector.equals("char") || imVector.equals("int") ||
//							imVector.equals("ImDrawIdx") || imVector.equals("ImWchar") || imVector.equals("ImGuiID")
//					) {
				infoMap.put(new Info("ImVector<" + imVector + ">::erase", "ImVector<" + imVector + ">::insert", "ImVector<" + imVector + ">::begin", "ImVector<" + imVector + ">::end").skip());
//			}
			// FIXME
			if (imVector.contains("::")) {
				infoMap.put(new Info("ImVector<" + imVector + ">::push_back").skip());
			}
		}
	}
}
