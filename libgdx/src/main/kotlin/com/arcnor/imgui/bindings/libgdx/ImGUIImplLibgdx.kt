package com.arcnor.imgui.bindings.libgdx

import com.arcnor.imgui.ImGUI
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL20.GL_ACTIVE_TEXTURE
import com.badlogic.gdx.graphics.GL20.GL_ARRAY_BUFFER
import com.badlogic.gdx.graphics.GL20.GL_ARRAY_BUFFER_BINDING
import com.badlogic.gdx.graphics.GL20.GL_BLEND
import com.badlogic.gdx.graphics.GL20.GL_BLEND_DST_ALPHA
import com.badlogic.gdx.graphics.GL20.GL_BLEND_DST_RGB
import com.badlogic.gdx.graphics.GL20.GL_BLEND_EQUATION_ALPHA
import com.badlogic.gdx.graphics.GL20.GL_BLEND_EQUATION_RGB
import com.badlogic.gdx.graphics.GL20.GL_BLEND_SRC_ALPHA
import com.badlogic.gdx.graphics.GL20.GL_BLEND_SRC_RGB
import com.badlogic.gdx.graphics.GL20.GL_CULL_FACE
import com.badlogic.gdx.graphics.GL20.GL_CURRENT_PROGRAM
import com.badlogic.gdx.graphics.GL20.GL_DEPTH_TEST
import com.badlogic.gdx.graphics.GL20.GL_ELEMENT_ARRAY_BUFFER
import com.badlogic.gdx.graphics.GL20.GL_ELEMENT_ARRAY_BUFFER_BINDING
import com.badlogic.gdx.graphics.GL20.GL_FLOAT
import com.badlogic.gdx.graphics.GL20.GL_FRAGMENT_SHADER
import com.badlogic.gdx.graphics.GL20.GL_FUNC_ADD
import com.badlogic.gdx.graphics.GL20.GL_ONE_MINUS_SRC_ALPHA
import com.badlogic.gdx.graphics.GL20.GL_SCISSOR_TEST
import com.badlogic.gdx.graphics.GL20.GL_SRC_ALPHA
import com.badlogic.gdx.graphics.GL20.GL_STREAM_DRAW
import com.badlogic.gdx.graphics.GL20.GL_TEXTURE0
import com.badlogic.gdx.graphics.GL20.GL_TEXTURE_2D
import com.badlogic.gdx.graphics.GL20.GL_TEXTURE_BINDING_2D
import com.badlogic.gdx.graphics.GL20.GL_TRIANGLES
import com.badlogic.gdx.graphics.GL20.GL_UNSIGNED_BYTE
import com.badlogic.gdx.graphics.GL20.GL_UNSIGNED_INT
import com.badlogic.gdx.graphics.GL20.GL_UNSIGNED_SHORT
import com.badlogic.gdx.graphics.GL20.GL_VERTEX_SHADER
import com.badlogic.gdx.graphics.GL20.GL_VIEWPORT
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.graphics.glutils.VertexData
import com.badlogic.gdx.utils.BufferUtils
import org.bytedeco.javacpp.BytePointer
import org.bytedeco.javacpp.IntPointer
import org.bytedeco.javacpp.Loader
import org.bytedeco.javacpp.Pointer

// FIXME: I think that returning WantCapture*() on the InputProcessor methods makes everything go 1 frame behind
class ImGUIImplLibgdx : InputProcessor {
	init {
		println("ImGUI Version: $VERSION")
	}

	protected companion object {
		// FIXME: This is actually NEEDED! (this initializes the JavaCPP ImGUI class, and it NEEDS to be on the companion, so it's the first thing called)
		private val VERSION = ImGUI.GetVersion().string

		private const val vertex_shader = """
		#ifdef GL_ES
		#define LOWP lowp
		#define MED mediump
		#define HIGH highp
		precision mediump float;
		#else
		#define MED
		#define LOWP
		#define HIGH
		#endif

		uniform mat4 ProjMtx;
		attribute vec2 Position;
		attribute vec2 UV;
		attribute vec4 Color;
		varying vec2 Frag_UV;
		varying vec4 Frag_Color;
		void main()
		{
			Frag_UV = UV;
			Frag_Color = Color;
			gl_Position = ProjMtx * vec4(Position.xy,0,1);
		}
		"""

		private const val fragment_shader = """
		#ifdef GL_ES
		#define LOWP lowp
		#define MED mediump
		#define HIGH highp
		precision mediump float;
		#else
		#define MED
		#define LOWP
		#define HIGH
		#endif

		uniform sampler2D Texture;
		varying vec2 Frag_UV;
		varying vec4 Frag_Color;
		void main()
		{
			gl_FragColor = Frag_Color * texture2D(Texture, Frag_UV.st);
		}
		"""
		val sizeOfImDrawVert = Loader.sizeof(ImGUI.ImDrawVert::class.java)
		val sizeOfImVec2 = Loader.sizeof(ImGUI.ImVec2::class.java)
		val sizeOfImDrawIdx = 2

		val offsetImDrawVertPos = Loader.offsetof(ImGUI.ImDrawVert::class.java, "pos")
		val offsetImDrawVertUV = Loader.offsetof(ImGUI.ImDrawVert::class.java, "uv")
		val offsetImDrawVertCol = Loader.offsetof(ImGUI.ImDrawVert::class.java, "col")

		private val IMVEC2_MINUS1 = ImGUI.ImVec2(-1f, -1f)
	}

	private val temp1 = BufferUtils.newIntBuffer(16)
	private val temp1_4 = BufferUtils.newIntBuffer(16)

	private var g_ShaderHandle = 0
	private var g_VertHandle = 0
	private var g_FragHandle = 0

	private var g_AttribLocationTex = 0
	private var g_AttribLocationProjMtx = 0
	private var g_AttribLocationPosition = 0
	private var g_AttribLocationUV = 0
	private var g_AttribLocationColor = 0

	private var g_VboHandle = 0
	private var g_ElementsHandle = 0

	private var g_FontTexture = 0
	private val g_MousePressed = booleanArrayOf(false, false, false, false, false, false, false, false)

	private lateinit var g_shader: ShaderProgram
	private lateinit var g_vbo: VertexData

	override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
		return ImGUI.GetIO().WantCaptureMouse()
	}

	override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
		return ImGUI.GetIO().WantCaptureMouse()
	}

	override fun keyTyped(character: Char): Boolean {
		val io = ImGUI.GetIO()
		io.AddInputCharacter(character.toShort())
		return io.WantCaptureKeyboard()
	}

	override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
		g_MousePressed[button] = true
		return ImGUI.GetIO().WantCaptureMouse()
	}

	override fun scrolled(amount: Int): Boolean {
		val io = ImGUI.GetIO()
		io.MouseWheel(amount * -0.1f)
		return io.WantCaptureMouse()
	}

	override fun keyUp(keycode: Int): Boolean {
		val io = ImGUI.GetIO()
		when (keycode) {
			Input.Keys.ALT_LEFT, Input.Keys.ALT_RIGHT -> io.KeyAlt(false)
			Input.Keys.CONTROL_LEFT, Input.Keys.CONTROL_RIGHT -> io.KeyCtrl(false)
			Input.Keys.SHIFT_LEFT, Input.Keys.SHIFT_RIGHT -> io.KeyShift(false)
			Input.Keys.SYM -> io.KeySuper(false)
			else -> io.KeysDown().put(keycode.toLong(), false)
		}
		return io.WantCaptureKeyboard()
	}

	override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
		return ImGUI.GetIO().WantCaptureMouse()
	}

	override fun keyDown(keycode: Int): Boolean {
		val io = ImGUI.GetIO()
		when (keycode) {
			Input.Keys.ALT_LEFT, Input.Keys.ALT_RIGHT -> io.KeyAlt(true)
			Input.Keys.CONTROL_LEFT, Input.Keys.CONTROL_RIGHT -> io.KeyCtrl(true)
			Input.Keys.SHIFT_LEFT, Input.Keys.SHIFT_RIGHT -> io.KeyShift(true)
			Input.Keys.SYM -> io.KeySuper(true)
			else -> io.KeysDown().put(keycode.toLong(), true)
		}
		return io.WantCaptureKeyboard()
	}

	fun init() {
		val io = ImGUI.GetIO()
		io.KeyMap(ImGUI.ImGuiKey_Tab, Input.Keys.TAB)                     // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
		io.KeyMap(ImGUI.ImGuiKey_LeftArrow, Input.Keys.LEFT)
		io.KeyMap(ImGUI.ImGuiKey_RightArrow, Input.Keys.RIGHT)
		io.KeyMap(ImGUI.ImGuiKey_UpArrow, Input.Keys.UP)
		io.KeyMap(ImGUI.ImGuiKey_DownArrow, Input.Keys.DOWN)
		io.KeyMap(ImGUI.ImGuiKey_PageUp, Input.Keys.PAGE_UP)
		io.KeyMap(ImGUI.ImGuiKey_PageDown, Input.Keys.PAGE_DOWN)
		io.KeyMap(ImGUI.ImGuiKey_Home, Input.Keys.HOME)
		io.KeyMap(ImGUI.ImGuiKey_End, Input.Keys.END)
		io.KeyMap(ImGUI.ImGuiKey_Delete, Input.Keys.FORWARD_DEL)
		io.KeyMap(ImGUI.ImGuiKey_Backspace, Input.Keys.BACKSPACE)
		io.KeyMap(ImGUI.ImGuiKey_Enter, Input.Keys.ENTER)
		io.KeyMap(ImGUI.ImGuiKey_Escape, Input.Keys.ESCAPE)
		io.KeyMap(ImGUI.ImGuiKey_A, Input.Keys.A)
		io.KeyMap(ImGUI.ImGuiKey_C, Input.Keys.C)
		io.KeyMap(ImGUI.ImGuiKey_V, Input.Keys.V)
		io.KeyMap(ImGUI.ImGuiKey_X, Input.Keys.X)
		io.KeyMap(ImGUI.ImGuiKey_Y, Input.Keys.Y)
		io.KeyMap(ImGUI.ImGuiKey_Z, Input.Keys.Z)

		io.SetClipboardTextFn(object : ImGUI.ImGuiIO.SetClipboardTextFn_Pointer_BytePointer() {
			override fun call(user_data: Pointer?, text: BytePointer) {
				Gdx.app.clipboard.contents = text.string
			}
		})
//		io.GetClipboardTextFn(callbackGetClipboardText)

//		GLFW.glfwSetMouseButtonCallback(window, callbackMouseButton);
//		GLFW.glfwSetScrollCallback(window, callbackScroll);
//		GLFW.glfwSetKeyCallback(window, callbackKey);
//		GLFW.glfwSetCharCallback(window, callbackChar);
//		GLFW.glfwSetWindowSizeCallback(window, callbackWindowSize);
//		GLFW.glfwSetFramebufferSizeCallback(window, callbackFramebufferSize);

		// Setup initial display size
		
		val g = Gdx.graphics
		val (display_w, display_h) = g.backBufferWidth to g.backBufferHeight
		val w = g.width.toFloat()
		val h = g.height.toFloat()
		io.DisplaySize(ImGUI.ImVec2(w, h))
		io.DisplayFramebufferScale(ImGUI.ImVec2(if (w > 0) (display_w / w) else 0f, if (h > 0) (display_h / h) else 0f))
	}

	fun resize(width: Int, height: Int) {
		val g = Gdx.graphics
		val (display_w, display_h) = g.backBufferWidth to g.backBufferHeight
		val io = ImGUI.GetIO()
		val w = width.toFloat()
		val h = height.toFloat()
		io.DisplaySize(ImGUI.ImVec2(w, h))
		io.DisplayFramebufferScale(ImGUI.ImVec2(if (w > 0) (display_w / w) else 0f, if (h > 0) (display_h / h) else 0f))
	}

	fun newFrame() {
		if (g_FontTexture == 0) {
			createDeviceObjects()
		}

		val io = ImGUI.GetIO()

		// Setup time step
		io.DeltaTime(Gdx.graphics.rawDeltaTime)

		// Setup inputs
		// (we already got mouse wheel, keyboard keys & characters from glfw callbacks polled in glfwPollEvents())
		// FIXME
		if (true/*GLFW.glfwGetWindowAttrib(g_Window, GLFW.GLFW_FOCUSED) != 0*/) {
//			GLFW.glfwGetCursorPos(g_Window, temp1d, temp2d);
			io.MousePos(ImGUI.ImVec2(Gdx.input.x.toFloat(), Gdx.input.y.toFloat()))   // Mouse position in screen coordinates (set to -1,-1 if no mouse / on another screen, etc.)
		} else {
			//io.MousePos(IMVEC2_MINUS1);
		}

		for (i in 0..2) {
			io.MouseDown().put(i.toLong(), g_MousePressed[i] || Gdx.input.isButtonPressed(i))    // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
			g_MousePressed[i] = false
		}

//		io.MouseWheel(g_MouseWheel.toFloat());
//		g_MouseWheel = 0f;

		// Hide OS mouse cursor if ImGui is drawing it
		if (Gdx.input.isCursorCatched != io.MouseDrawCursor()) {
			// Setting this continuously causes GDX to skip mouse input almost every frame
			Gdx.input.isCursorCatched = io.MouseDrawCursor()
		}
//		GLFW.glfwSetInputMode(g_Window, GLFW.GLFW_CURSOR, if (io.MouseDrawCursor()) GLFW.GLFW_CURSOR_HIDDEN else GLFW.GLFW_CURSOR_NORMAL);

		// Start the frame
		ImGUI.NewFrame()
	}

	private fun createDeviceObjects(): Boolean {
		val gl = Gdx.gl

		// Backup GL state
		val last_texture = gl.glGetInteger(GL_TEXTURE_BINDING_2D)
		val last_array_buffer = gl.glGetInteger(GL_ARRAY_BUFFER_BINDING)
//		val last_vertex_array = gl.glGetInteger(GL_VERTEX_ARRAY_BINDING);

		g_shader = ShaderProgram(vertex_shader, fragment_shader)
		g_ShaderHandle = gl.glCreateProgram()
		g_VertHandle = gl.glCreateShader(GL_VERTEX_SHADER)
		g_FragHandle = gl.glCreateShader(GL_FRAGMENT_SHADER)
		gl.glShaderSource(g_VertHandle, vertex_shader)
		gl.glShaderSource(g_FragHandle, fragment_shader)
		gl.glCompileShader(g_VertHandle)
		gl.glCompileShader(g_FragHandle)
		gl.glAttachShader(g_ShaderHandle, g_VertHandle)
		gl.glAttachShader(g_ShaderHandle, g_FragHandle)
		gl.glLinkProgram(g_ShaderHandle)

		g_AttribLocationTex = gl.glGetUniformLocation(g_ShaderHandle, "Texture")
		g_AttribLocationProjMtx = gl.glGetUniformLocation(g_ShaderHandle, "ProjMtx")
		g_AttribLocationPosition = gl.glGetAttribLocation(g_ShaderHandle, "Position")
		g_AttribLocationUV = gl.glGetAttribLocation(g_ShaderHandle, "UV")
		g_AttribLocationColor = gl.glGetAttribLocation(g_ShaderHandle, "Color")

//		g_vbo = if (Gdx.gl30 != null) {
//			return VertexBufferObjectWithVAO(false, 4096, vertexAttributes)
//		} else {
//			return VertexBufferObject(false, maxVertices, vertexAttributes)
//		}
		gl.glGenBuffers(2, temp1_4)
		g_VboHandle = temp1_4[0]
		g_ElementsHandle = temp1_4[1]

//		g_VaoHandle = gl.glGenVertexArrays();
//		gl.glBindVertexArray(g_VaoHandle);
//		gl.glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);

//		gl.glEnableVertexAttribArray(g_AttribLocationPosition);
//		gl.glEnableVertexAttribArray(g_AttribLocationUV);
//		gl.glEnableVertexAttribArray(g_AttribLocationColor);
//
//		gl.glVertexAttribPointer(g_AttribLocationPosition, 2, GL_FLOAT, false, sizeOfImDrawVert, Loader.offsetof(ImGUI.ImDrawVert::class.java, "pos"));
//		gl.glVertexAttribPointer(g_AttribLocationUV, 2, GL_FLOAT, false, sizeOfImDrawVert, Loader.offsetof(ImGUI.ImDrawVert::class.java, "uv"));
//		gl.glVertexAttribPointer(g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, true, sizeOfImDrawVert, Loader.offsetof(ImGUI.ImDrawVert::class.java, "col"));

		// Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.
		createFontsTexture(rgba = true)

		// Restore modified GL state
		gl.glBindTexture(GL_TEXTURE_2D, last_texture)
		gl.glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer)
//		gl.glBindVertexArray(last_vertex_array);

		return true
	}

	private fun createFontsTexture(rgba: Boolean): Boolean {
		// Build texture atlas
		val io = ImGUI.GetIO()
		val pixels = BytePointer(1024 * 1024 * 4)
		val w = IntPointer(4)
		val h = IntPointer(4)

		if (rgba) {
			io.Fonts().GetTexDataAsRGBA32(pixels, w, h)
		} else {
			io.Fonts().GetTexDataAsAlpha8(pixels, w, h)
		}

		// Upload texture to graphics system
		val gl = Gdx.gl
		gl.glGetIntegerv(GL20.GL_TEXTURE_BINDING_2D, temp1)
		val last_texture = temp1[0]
		gl.glGenTextures(1, temp1)
		g_FontTexture = temp1[0]
		gl.glBindTexture(GL20.GL_TEXTURE_2D, g_FontTexture)
		gl.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_LINEAR)
		gl.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_LINEAR)
		val width = w.get()
		val height = h.get()
		val type = if (rgba) GL20.GL_RGBA else GL20.GL_ALPHA
		gl.glTexImage2D(GL20.GL_TEXTURE_2D, 0, type, width, height, 0, type, GL20.GL_UNSIGNED_BYTE, pixels.asBuffer())

		// Store our identifier
		io.Fonts().TexID(CastPointer(g_FontTexture.toLong()))

		// Restore state
		gl.glBindTexture(GL20.GL_TEXTURE_2D, last_texture)

		return true
	}
	
	fun render() {
		ImGUI.Render()

		val draw_data = ImGUI.GetDrawData()

		// Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
		val io = ImGUI.GetIO()
		val displaySize = io.DisplaySize()
		val displayFramebufferScale = io.DisplayFramebufferScale()

		val fb_width = (displaySize.x() * displayFramebufferScale.x()).toInt()
		val fb_height = (displaySize.y() * displayFramebufferScale.y()).toInt()
		if (fb_width == 0 || fb_height == 0)
			return
		draw_data.ScaleClipRects(displayFramebufferScale)

		val gl = Gdx.gl

		// Backup GL state
		val last_program = gl.glGetInteger(GL_CURRENT_PROGRAM)
		val last_texture = gl.glGetInteger(GL_TEXTURE_BINDING_2D)
		val last_active_texture = gl.glGetInteger(GL_ACTIVE_TEXTURE)
		val last_array_buffer = gl.glGetInteger(GL_ARRAY_BUFFER_BINDING)
		val last_element_array_buffer = gl.glGetInteger(GL_ELEMENT_ARRAY_BUFFER_BINDING)
//		val last_vertex_array = gl.glGetInteger(GL_VERTEX_ARRAY_BINDING)
		val last_blend_src_rgb = gl.glGetInteger(GL_BLEND_SRC_RGB)
		val last_blend_src_alpha = gl.glGetInteger(GL_BLEND_SRC_ALPHA)
		val last_blend_dst_rgb = gl.glGetInteger(GL_BLEND_DST_RGB)
		val last_blend_dst_alpha = gl.glGetInteger(GL_BLEND_DST_ALPHA)
		val last_blend_equation_rgb = gl.glGetInteger(GL_BLEND_EQUATION_RGB)
		val last_blend_equation_alpha = gl.glGetInteger(GL_BLEND_EQUATION_ALPHA)

		gl.glGetIntegerv(GL_VIEWPORT, temp1_4)
		val last_viewport = temp1_4

		val last_enable_blend = gl.glIsEnabled(GL_BLEND)
		val last_enable_cull_face = gl.glIsEnabled(GL_CULL_FACE)
		val last_enable_depth_test = gl.glIsEnabled(GL_DEPTH_TEST)
		val last_enable_scissor_test = gl.glIsEnabled(GL_SCISSOR_TEST)

		// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
		gl.glEnable(GL_BLEND)
		gl.glBlendEquation(GL_FUNC_ADD)
		gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		gl.glDisable(GL_CULL_FACE)
		gl.glDisable(GL_DEPTH_TEST)
		gl.glEnable(GL_SCISSOR_TEST)
		gl.glActiveTexture(GL_TEXTURE0)

		// Setup viewport, orthographic projection matrix
		gl.glViewport(0, 0, fb_width, fb_height)
		// FIXME: Convert to FloatBuffer
		val ortho_projection = floatArrayOf(
				2.0f / io.DisplaySize().x(), 0.0f, 0.0f, 0.0f,
				0.0f, 2.0f / -io.DisplaySize().y(), 0.0f, 0.0f,
				0.0f, 0.0f, -1.0f, 0.0f,
				-1.0f, 1.0f, 0.0f, 1.0f
		)
		gl.glUseProgram(g_ShaderHandle)
		gl.glUniform1i(g_AttribLocationTex, 0)
		gl.glUniformMatrix4fv(g_AttribLocationProjMtx, 1, false, ortho_projection, 0)

		gl.glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle)
		gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ElementsHandle)

		gl.glEnableVertexAttribArray(g_AttribLocationPosition)
		gl.glEnableVertexAttribArray(g_AttribLocationUV)
		gl.glEnableVertexAttribArray(g_AttribLocationColor)

		gl.glVertexAttribPointer(g_AttribLocationPosition, 2, GL_FLOAT, false, sizeOfImDrawVert, offsetImDrawVertPos)
		gl.glVertexAttribPointer(g_AttribLocationUV, 2, GL_FLOAT, false, sizeOfImDrawVert, offsetImDrawVertUV)
		gl.glVertexAttribPointer(g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, true, sizeOfImDrawVert, offsetImDrawVertCol)

		// Render command lists
		for (n in 0..draw_data.CmdListsCount() - 1) {
			val cmd_list = draw_data.CmdLists(n)
			var idx_buffer_offset = 0

			val tmpPtr = BytePointer(CastPointer(cmd_list.VtxBuffer().front().address(), (cmd_list.VtxBuffer().size() * sizeOfImDrawVert).toLong()))
			gl.glBufferData(GL_ARRAY_BUFFER, tmpPtr.limit().toInt(), tmpPtr.asBuffer(), GL_STREAM_DRAW)

			val tmpPtr2 = BytePointer(CastPointer(cmd_list.IdxBuffer().front().address(), (cmd_list.IdxBuffer().size() * sizeOfImDrawIdx).toLong()))
			gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, tmpPtr2.limit().toInt(), tmpPtr2.asBuffer(), GL_STREAM_DRAW)

			for (cmd_i in 0..cmd_list.CmdBuffer().size() - 1) {
				val pcmd = cmd_list.CmdBuffer().get(cmd_i)

				if (false/*pcmd->UserCallback*/) {
					pcmd.UserCallback().call(cmd_list, pcmd)
				} else {
					gl.glBindTexture(GL_TEXTURE_2D, pcmd.TextureId().address().toInt())
					val clipRect = pcmd.ClipRect()
					val x = clipRect.x()
					val y = clipRect.y()
					val z = clipRect.z()
					val w = clipRect.w()
					gl.glScissor(
							x.toInt(),
							(fb_height - w).toInt(),
							(z - x).toInt(),
							(w - y).toInt()
					)

					gl.glDrawElements(GL_TRIANGLES, pcmd.ElemCount(), if (sizeOfImDrawIdx == 2) GL_UNSIGNED_SHORT else GL_UNSIGNED_INT, idx_buffer_offset)
				}
				idx_buffer_offset += pcmd.ElemCount() * sizeOfImDrawIdx
			}
		}

		gl.glDisableVertexAttribArray(g_AttribLocationPosition)
		gl.glDisableVertexAttribArray(g_AttribLocationUV)
		gl.glDisableVertexAttribArray(g_AttribLocationColor)

		// Restore modified state
		gl.glUseProgram(last_program)
		gl.glActiveTexture(last_active_texture)
		gl.glBindTexture(GL_TEXTURE_2D, last_texture)
//		gl.glBindVertexArray(last_vertex_array);
		gl.glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer)
		gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer)
		gl.glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha)
		gl.glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb, last_blend_src_alpha, last_blend_dst_alpha)
		if (last_enable_blend) gl.glEnable(GL_BLEND); else gl.glDisable(GL_BLEND)
		if (last_enable_cull_face) gl.glEnable(GL_CULL_FACE); else gl.glDisable(GL_CULL_FACE)
		if (last_enable_depth_test) gl.glEnable(GL_DEPTH_TEST); else gl.glDisable(GL_DEPTH_TEST)
		if (last_enable_scissor_test) gl.glEnable(GL_SCISSOR_TEST); else gl.glDisable(GL_SCISSOR_TEST)
		gl.glViewport(last_viewport[0], last_viewport[1], last_viewport[2], last_viewport[3])
	}

	fun GL20.glGetInteger(pname: Int): Int {
		glGetIntegerv(pname, temp1)
		return temp1[0]
	}

	fun shutdown() {
		invalidateDeviceObjects()
		ImGUI.Shutdown()
	}

	fun invalidateDeviceObjects() {
		val gl = Gdx.gl
		if (g_VboHandle != 0) gl.glDeleteBuffer(g_VboHandle)
		if (g_ElementsHandle != 0) gl.glDeleteBuffer(g_ElementsHandle)

		g_VboHandle = 0
		g_ElementsHandle = 0

		gl.glDetachShader(g_ShaderHandle, g_VertHandle)
		gl.glDeleteShader(g_VertHandle)
		g_VertHandle = 0

		gl.glDetachShader(g_ShaderHandle, g_FragHandle)
		gl.glDeleteShader(g_FragHandle)
		g_FragHandle = 0

		gl.glDeleteProgram(g_ShaderHandle)
		g_ShaderHandle = 0

		if (g_FontTexture != 0) {
			gl.glDeleteTexture(g_FontTexture)
			ImGUI.GetIO().Fonts().TexID(CastPointer(0))
			g_FontTexture = 0
		}
	}
}

class CastPointer(address: Long, capacity: Long = 0) : Pointer() {
	init {
		this.address = address
		this.limit = capacity
		this.capacity = capacity
	}
}

