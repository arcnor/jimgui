/**
 * Copyright 2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arcnor.imgui.bindings.libgdx

import com.arcnor.imgui.ImGUI
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Affine2
import org.bytedeco.javacpp.FloatPointer

class LibgdxExample {
	companion object {
		@JvmStatic fun main(args: Array<String>) {
			// Initialize lib
			println("Version: " + ImGUI.GetVersion().string + " " + ImGUI.IMGUI_VERSION)

			val config = Lwjgl3ApplicationConfiguration()
			config.setWindowedMode(1280, 768)
			config.setResizable(true)
			config.setTitle("ImGUI LibGDX example")
			Lwjgl3Application(LibgdxImpl(), config)

			// Initialize lib
//			println("Version: " + ImGUI.GetVersion());

//			Lwjgl3Example(ImGUIImplLwjgl3GL2(), isOpenGL3 = false).run()
		}
	}
}

class LibgdxImpl : ApplicationAdapter() {
	private val imGUISupport = ImGUIImplLibgdx()

	private var fX = FloatPointer(1);
	private var fY = FloatPointer(1);
	private var fRot = FloatPointer(1);
	private val clear_color = ImGUI.ImColor(114, 144, 154).asImVec4()
	private var show_test_window = false
	private var show_another_window = false

	private lateinit var batch: SpriteBatch

	private lateinit var texLogo: Texture

	override fun create() {
		Gdx.input.inputProcessor = imGUISupport

		imGUISupport.init()

		batch = SpriteBatch()
		texLogo = Texture("logo.png")
		texLogo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
	}

	override fun render() {
		val gl = Gdx.gl

		imGUISupport.newFrame();

		// 1. Show a simple window
		// Tip: if we don't call ImGUI.Begin()/ImGUI.End() the widgets appears in a window automatically called "Debug"
		run {
			ImGUI.Text("Hello LibGDX world!");
			ImGUI.SliderFloat("x", fX, 0.0f, 1.0f);
			ImGUI.SliderFloat("y", fY, 0.0f, 1.0f);
			ImGUI.SliderFloat("rot", fRot, 0.0f, 360.0f);
			ImGUI.ColorEdit3("clear color", FloatPointer(CastPointer(clear_color.address())));
			if (ImGUI.Button("Test Window")) {show_test_window = !show_test_window}
			if (ImGUI.Button("Another Window")) show_another_window = !show_another_window;
			ImGUI.Text(String.format("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGUI.GetIO().Framerate(), ImGUI.GetIO().Framerate()));
		}

		// 2. Show another simple window, this time using an explicit Begin/End pair
		if (show_another_window)
		{
			ImGUI.SetNextWindowSize(ImGUI.ImVec2(200f, 100f), ImGUI.ImGuiSetCond_FirstUseEver);
			ImGUI.Begin("Another Window", booleanArrayOf(show_another_window), 0);
			ImGUI.Text("Hello");
			ImGUI.End();
		}

		// 3. Show the ImGui test window. Most of the sample code is in ImGUI.ShowTestWindow()
		if (show_test_window)
		{
			ImGUI.SetNextWindowPos(ImGUI.ImVec2(650f, 20f), ImGUI.ImGuiSetCond_FirstUseEver);
			ImGUI.ShowTestWindow(booleanArrayOf(show_test_window));
		}

		gl.glClearColor(clear_color.x(), clear_color.y(), clear_color.z(), clear_color.w());
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT);

		batch.begin()
		val transform = Affine2()
		transform.idt()
		transform.rotate(fRot.get())
		batch.draw(texLogo,
				Gdx.graphics.width * fX.get(), Gdx.graphics.height * fY.get(),
				texLogo.width/2f, texLogo.height/2f, texLogo.width.toFloat(), texLogo.height.toFloat(),
				1f, 1f, fRot.get(), 0, 0, texLogo.width, texLogo.height,
				false, false
		)
		batch.end()

		// Rendering

		imGUISupport.render()
	}

	override fun resize(width: Int, height: Int) {
		println("Resize!! $width x $height")
		imGUISupport.resize(width, height)
	}

	override fun dispose() {
		imGUISupport.shutdown()
	}

	override fun pause() {
		println("pause")
	}

	override fun resume() {
		println("resume")
	}
}

